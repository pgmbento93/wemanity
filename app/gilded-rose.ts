export class Item {
    name: string;
    sellIn: number;
    quality: number;

    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

export class GildedRose {
    items: Array<Item>;

    constructor(items = [] as Array<Item>) {
        this.items = items;
    }
 
    // The Quality of an item is never more than 50
    // The Quality of an item is never negative
    forceQuatityRange(quantity: number): number {
        quantity = quantity > 50 ? quantity = 50 : quantity
        quantity = quantity < 0 ? quantity = 0 : quantity
        return quantity
    }

    // Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less 
    increaseQuality(item: Item, condition: number, increase1: number, increase2: number): number {
        item.quality = item.sellIn <= condition ? item.quality + increase1 : item.quality + increase2
        return item.quality
    }

    decreaseQuality(item: Item, increase1: number, increase2: number): number {
        item.quality = item.sellIn < 0 ? item.quality - increase1 : item.quality - increase2
        return item.quality
    }


    updateQuality() {
        for (let item of this.items) {
            item.sellIn = item.sellIn - 1

            switch (item.name) {
                case "Aged Brie": { 
                    //"Aged Brie" actually increases in Quality the older it gets
                    item.quality += 1
                    break;
                }
                case "Backstage passes to a TAFKAL80ETC concert": {
                    // But quality drops to 0 after the concert
                    if (item.sellIn <= 0) {
                        item.quality = 0
                    }
                    else {
                        item.quality = this.increaseQuality(item, 10, 2, 1)
                        item.quality = this.increaseQuality(item, 5, 1, 0)
                    }
                    break;
                }
                case "Sulfuras, Hand of Ragnaros": {
                    //"Sulfuras", being a legendary item, never has to be sold or decreases in Quality
                    item.sellIn = 0 
                    break;
                }
                case "Conjured Mana Cake": {
                    //"Conjured" items degrade in Quality twice as fast as normal items
                    item.quality = this.decreaseQuality(item, 4, 2)
                    break;
                }
                default: {
                    //At the end of each day our system lowers both values for every item
                    //Once the sell by date has passed, Quality degrades twice as fast
                    item.quality = this.decreaseQuality(item, 2, 1)
                    break;
                }
            }
            item.quality = item.name=="Sulfuras, Hand of Ragnaros"? item.quality: this.forceQuatityRange(item.quality)
        }
        return this.items;
    }
}
